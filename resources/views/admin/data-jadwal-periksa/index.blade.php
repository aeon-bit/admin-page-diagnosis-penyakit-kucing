@extends('admin.layouts.main')
@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-3 text-gray-800">Data Jadwal Periksa</h1>

    {{-- alert --}}
    @include('admin.alerts.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{ route('data-jadwal-periksa.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nama Kucing</th>
                            <th>Nama Pemilik</th>
                            <th>No. HP</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dataJadwalPeriksa as $item)
                            <tr>
                                <td>{{ $loop->iteration }}.</td>
                                <td>{{ date('d-m-Y', strtotime($item->tanggal_periksa)) }}</td>
                                <td>{{ $item->user->nama_kucing }}</td>
                                <td>{{ $item->user->nama }}</td>
                                <td>{{ $item->user->no_hp }}</td>
                                <td class="{{ $item->dataRiwayatDiagnosa !== null ? 'text-success' : 'text-danger' }}">
                                    {{ $item->dataRiwayatDiagnosa !== null ? 'Sudah' : 'Belum' }}
                                </td>
                                <td>
                                    <form class="d-inline" action="{{ route('data-jadwal-periksa.destroy', $item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm rounded-circle border-0" onclick="return confirm('Apakah anda yakin ?')"><i class="fas fa-trash fa-sm text-white-100"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection