@extends('admin.layouts.main')
@section('content')
<div class="mx-5 my-5">
    <a href="{{ route('data-jadwal-periksa.index') }}" class="text-secondary">
        <h6 class="m-0 font-weight-bold"><i class="fas fa-chevron-left"></i> Kembali</h6>
    </a>
</div>

<div class="card shadow mt-3 mx-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Jadwal Periksa</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('data-jadwal-periksa.store') }}" method="post">
            @csrf
            
            <div class="form-group">
                <label for="tanggal_periksa">Tanggal Periksa</label>
                <input type="date" class="form-control @error('tanggal_periksa') is-invalid @enderror" id="tanggal_periksa" name="tanggal_periksa" value="{{ old('tanggal_periksa') }}" required>
                @error('tanggal_periksa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary ">Simpan</button>
        </form>
    </div>
</div>
@endsection