@extends('admin.layouts.main')
@section('content')
<div class="card shadow mb-4">
    <div class="card-body">
        <h4 class="mt-2">Hi, <strong>{{ auth()->user()->nama }}</strong>. Selamat datang di halaman Administrator!</h4>
    </div>
</div>
@endsection