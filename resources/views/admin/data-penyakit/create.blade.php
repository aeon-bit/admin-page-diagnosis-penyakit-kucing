@extends('admin.layouts.main')
@section('content')
<div class="mx-5 my-5">
    <a href="{{ route('data-penyakit.index') }}" class="text-secondary">
        <h6 class="m-0 font-weight-bold"><i class="fas fa-chevron-left"></i> Kembali</h6>
    </a>
</div>

<div class="card shadow mt-3 mx-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Penyakit</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('data-penyakit.store') }}" method="post">
            @csrf
            
            <div class="form-group">
                <label for="kode_penyakit">Kode Penyakit</label>
                <input type="text" class="form-control @error('kode_penyakit') is-invalid @enderror" id="kode_penyakit" name="kode_penyakit" value="{{ old('kode_penyakit') }}" placeholder="Masukkan Kode Penyakit" required>
                @error('kode_penyakit')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_penyakit">Nama Penyakit</label>
                <input type="text" class="form-control @error('nama_penyakit') is-invalid @enderror" id="nama_penyakit" name="nama_penyakit" value="{{ old('nama_penyakit') }}" placeholder="Masukkan Nama Penyakit" required>
                @error('nama_penyakit')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea id="deskripsi" class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" rows="4" cols="50" placeholder="Masukkan Deskripsi">{{ old('deskripsi') }}</textarea>
                @error('deskripsi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="pengobatan">Pengobatan</label>
                <textarea id="pengobatan" class="form-control @error('pengobatan') is-invalid @enderror" name="pengobatan" rows="4" cols="50" placeholder="Masukkan Pengobatan">{{ old('pengobatan') }}</textarea>
                @error('pengobatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary ">Simpan</button>
        </form>
    </div>
</div>
@endsection