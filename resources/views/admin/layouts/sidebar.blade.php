<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    {{-- <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a> --}}
    <div class="mt-5 mb-3"></div>

    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ request()->is('/*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('beranda') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Beranda</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('data-penyakit*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('data-penyakit.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Penyakit</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('data-gejala*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('data-gejala.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Gejala</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('data-jadwal-periksa*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('data-jadwal-periksa.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Jadwal Periksa</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('data-riwayat-diagnosa*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('data-riwayat-diagnosa.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Riwayat Diagnosa</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('data-artikel*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('data-artikel.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Artikel</span>
        </a>
    </li>

</ul>