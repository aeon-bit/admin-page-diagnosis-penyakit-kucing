@extends('admin.layouts.main')
@section('content')
<div class="mx-5 my-5">
    <a href="{{ route('data-riwayat-diagnosa.index') }}" class="text-secondary">
        <h6 class="m-0 font-weight-bold"><i class="fas fa-chevron-left"></i> Kembali</h6>
    </a>
</div>

<div class="card shadow mt-3 mx-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Diagnosa</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('data-riwayat-diagnosa.store') }}" method="post">
            @csrf
            
            <div class="form-group">
                <label for="data_jadwal_periksa_id">Pilih Pasien</label>
                <select class="custom-select  @error('data_jadwal_periksa_id') is-invalid @enderror" name="data_jadwal_periksa_id" id="data_jadwal_periksa_id" required>
                    <option selected disabled>Pilih Pasien</option>
                    @foreach ($dataJadwalPeriksa as $item)
                        <option value="{{ $item->id }}">{{ date('d-m-Y', strtotime($item->tanggal_periksa)) }} - {{ $item->user->nama_kucing }}</option>
                    @endforeach
                </select> 
                @error('data_jadwal_periksa_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror   
            </div>
            <div class="form-group">
                <label for="gejala">Pilih Gejala</label>
                <div class="row">
                    @foreach ($dataGejala as $item)
                        <div class="col-md-3">
                            <input type="checkbox" name="gejala[]" id="gejala" value="{{ $item->nama_gejala }}"> {{ $item->nama_gejala }}
                        </div>
                    @endforeach
                </div>
                @error('gejala')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="hasil_diagnosa">Hasil Diagnosa</label>
                <input type="text" class="form-control @error('hasil_diagnosa') is-invalid @enderror" id="hasil_diagnosa" name="hasil_diagnosa" value="{{ old('hasil_diagnosa') }}" placeholder="Masukkan Nama Gejala" required>
                @error('hasil_diagnosa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="kesimpulan_diagnosa">Kesimpulan Diagnosa</label>
                <textarea id="kesimpulan_diagnosa" class="form-control @error('kesimpulan_diagnosa') is-invalid @enderror" name="kesimpulan_diagnosa" rows="4" cols="50" placeholder="Masukkan Kesimpulan Diagnosa">{{ old('kesimpulan_diagnosa') }}</textarea>
                @error('kesimpulan_diagnosa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="saran_pengobatan">Saran Pengobatan</label>
                <textarea id="saran_pengobatan" class="form-control @error('saran_pengobatan') is-invalid @enderror" name="saran_pengobatan" rows="4" cols="50" placeholder="Masukkan Saran Pengobatan">{{ old('saran_pengobatan') }}</textarea>
                @error('saran_pengobatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary ">Simpan</button>
        </form>
    </div>
</div>
@endsection