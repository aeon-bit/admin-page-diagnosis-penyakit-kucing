@extends('admin.layouts.main')
@section('content')
<div class="mx-5 my-5">
    <a href="{{ route('data-riwayat-diagnosa.index') }}" class="text-secondary">
        <h6 class="m-0 font-weight-bold"><i class="fas fa-chevron-left"></i> Kembali</h6>
    </a>
</div>

<div class="card shadow mt-3 mx-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Hasil Diagnosa</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Tanggal Periksa</td>
                                    <td>:</td>
                                    <td>{{ date('d-m-Y', strtotime($dataRiwayatDiagnosa->dataJadwalPeriksa->tanggal_periksa)) }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Pemilik</td>
                                    <td>:</td>
                                    <td>{{ $dataRiwayatDiagnosa->dataJadwalPeriksa->user->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Kucing</td>
                                    <td>:</td>
                                    <td>{{ $dataRiwayatDiagnosa->dataJadwalPeriksa->user->nama_kucing }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Kucing</td>
                                    <td>:</td>
                                    <td>{{ $dataRiwayatDiagnosa->dataJadwalPeriksa->user->jenis_kucing }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr style="margin: -15px 0 5px 0">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <p>Gejala Yang Di Derita :</p>
                        <ol class="pl-4">
                            @foreach ($dataRiwayatDiagnosa['gejala'] as $item)
                                <li class="mb-2">{{ $item['gejala'] }}</li>    
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mt-5 mx-5 my-4 mb-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Kesimpulan Diagnosa</h6>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-body">
                <p>{{ $dataRiwayatDiagnosa->kesimpulan_diagnosa }}</p>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mt-5 mx-5 my-4 mb-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Saran Pengobatan</h6>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-body">
                <p>{{ $dataRiwayatDiagnosa->saran_pengobatan }}</p>
            </div>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end my-5 mx-5">
    <div>
        <a href="{{ route('data-riwayat-diagnosa.pdf', $dataRiwayatDiagnosa->id) }}" class="btn btn-sm btn-primary">Cetak</a>
    </div>
</div>
@endsection