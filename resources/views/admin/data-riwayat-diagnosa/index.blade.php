@extends('admin.layouts.main')
@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-3 text-gray-800">Data Riwayat Diagnosa</h1>

    {{-- alert --}}
    @include('admin.alerts.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{ route('data-riwayat-diagnosa.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Kucing</th>
                            <th>Nama Pemilik</th>
                            <th>Hasil</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dataRiwayatDiagnosa as $item)
                            <tr>
                                <td>{{ $loop->iteration }}.</td>
                                <td>{{ $item->dataJadwalPeriksa->user->nama_kucing }}</td>
                                <td>{{ $item->dataJadwalPeriksa->user->nama }}</td>
                                <td>{{ $item->hasil_diagnosa }}</td>
                                <td>
                                    <a href="{{ route('data-riwayat-diagnosa.show', $item->id) }}" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm rounded-circle border-0">
                                        <i class="fas fa-info-circle fa-sm text-white-100"></i> 
                                    </a>
                                    <a href="{{ route('data-riwayat-diagnosa.pdf', $item->id) }}" class="d-none d-sm-inline-block pl-3">
                                        Cetak
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection