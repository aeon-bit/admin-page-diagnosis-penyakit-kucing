@extends('admin.layouts.main')
@section('content')
<div class="mx-5 my-5">
    <a href="{{ route('data-artikel.index') }}" class="text-secondary">
        <h6 class="m-0 font-weight-bold"><i class="fas fa-chevron-left"></i> Kembali</h6>
    </a>
</div>

<div class="card shadow mt-3 mx-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Artikel</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('data-artikel.update', $dataArtikel->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            
            <div class="form-group">
                <label for="judul_artikel">Judul Artikel</label>
                <input type="text" class="form-control @error('judul_artikel') is-invalid @enderror" id="judul_artikel" name="judul_artikel" value="{{ old('judul_artikel', $dataArtikel->judul_artikel) }}" placeholder="Masukkan Judul Artikel" required>
                @error('judul_artikel')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea id="deskripsi" class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" rows="4" cols="50" placeholder="Masukkan Deskripsi">{{ old('deskripsi', $dataArtikel->deskripsi) }}</textarea>
                @error('deskripsi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group pt-3">
                <label for="gambar">Gambar</label>
                <input type="file" class=" @error('gambar') is-invalid @enderror" id="gambar" name="gambar" onchange="previewImage()" value="{{ old('gambar', $dataArtikel->gambar) }}">
                @error('gambar')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <img src="{{ asset('storage/images/'.$dataArtikel->gambar) }}" alt="" class="d-block img-preview mb-3" width="300px">
            
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
@endsection

@push('script')
    <script>
        // preview image
        function previewImage() {
            const image = document.querySelector('#gambar');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endpush