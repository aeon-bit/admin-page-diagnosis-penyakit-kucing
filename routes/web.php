<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\DataArtikelController;
use App\Http\Controllers\DataGejalaController;
use App\Http\Controllers\DataJadwalPeriksaController;
use App\Http\Controllers\DataPenyakitController;
use App\Http\Controllers\DataRiwayatDiagnosaController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function(){
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login', [AuthController::class, 'store'])->name('store');
});

Route::middleware(['auth', 'isAdmin'])->group(function(){
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('/', BerandaController::class)->name('beranda');

    Route::group(['prefix' => 'data-penyakit', 'as' => 'data-penyakit.'], function() {
        Route::get('/', [DataPenyakitController::class, 'index'])->name('index');
        Route::get('/tambah-data', [DataPenyakitController::class, 'create'])->name('create');
        Route::post('/tambah-data', [DataPenyakitController::class, 'store'])->name('store');
        Route::get('/{dataPenyakit}/edit', [DataPenyakitController::class, 'edit'])->name('edit');
        Route::put('/{dataPenyakit}/edit', [DataPenyakitController::class, 'update'])->name('update');
        Route::delete('/{dataPenyakit}/delete', [DataPenyakitController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'data-gejala', 'as' => 'data-gejala.'], function(){
        Route::get('/', [DataGejalaController::class, 'index'])->name('index');
        Route::get('/tambah-data', [DataGejalaController::class, 'create'])->name('create');
        Route::post('/tambah-data', [DataGejalaController::class, 'store'])->name('store');
        Route::get('/{dataGejala}/edit', [DataGejalaController::class, 'edit'])->name('edit');
        Route::put('/{dataGejala}/edit', [DataGejalaController::class, 'update'])->name('update');
        Route::delete('/{dataGejala}/delete', [DataGejalaController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'data-jadwal-periksa', 'as' => 'data-jadwal-periksa.'], function() {
        Route::get('/', [DataJadwalPeriksaController::class, 'index'])->name('index');
        Route::get('/tambah-data', [DataJadwalPeriksaController::class, 'create'])->name('create');
        Route::post('/tambah-data', [DataJadwalPeriksaController::class, 'store'])->name('store');
        Route::delete('/{dataJadwalPeriksa}/delete', [DataJadwalPeriksaController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'data-riwayat-diagnosa', 'as' => 'data-riwayat-diagnosa.'], function() {
        Route::get('/', [DataRiwayatDiagnosaController::class, 'index'])->name('index');
        Route::get('/tambah-data', [DataRiwayatDiagnosaController::class, 'create'])->name('create');
        Route::post('/tambah-data', [DataRiwayatDiagnosaController::class, 'store'])->name('store');
        Route::get('/{dataRiwayatDiagnosa}/detail', [DataRiwayatDiagnosaController::class, 'show'])->name('show');
        Route::get('/{dataRiwayatDiagnosa}/pdf', [DataRiwayatDiagnosaController::class, 'pdf'])->name('pdf');
    });

    Route::group(['prefix' => 'data-artikel', 'as' => 'data-artikel.'], function() {
        Route::get('/', [DataArtikelController::class, 'index'])->name('index');
        Route::get('/create', [DataArtikelController::class, 'create'])->name('create');
        Route::post('/create', [DataArtikelController::class, 'store'])->name('store');
        Route::get('/{dataArtikel}/edit', [DataArtikelController::class, 'edit'])->name('edit');
        Route::put('/{dataArtikel}/edit', [DataArtikelController::class, 'update'])->name('update');
        Route::delete('/{dataArtikel}/delete', [DataArtikelController::class, 'destroy'])->name('destroy');
    });
});



