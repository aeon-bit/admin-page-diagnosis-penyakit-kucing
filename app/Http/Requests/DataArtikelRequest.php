<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataArtikelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'judul_artikel' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required'
        ];

        if (request()->isMethod('PUT')) {
            $rules['gambar'] = '';
        }
       
        return $rules;
    }
}
