<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataGejalaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'kode_gejala' => 'required',
            'nama_gejala' => 'required',
        ];

        if (!request()->isMethod('PUT')) {
            $rules['kode_gejala'] = 'required|unique:data_gejala,kode_gejala';
        }
       
        return $rules;
    }
}
