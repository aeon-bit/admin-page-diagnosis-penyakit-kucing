<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataPenyakitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'kode_penyakit' => 'required',
            'nama_penyakit' => 'required',
            'deskripsi' => 'required',
            'pengobatan' => 'required'
        ];

        if (!request()->isMethod('PUT')) {
            $rules['kode_penyakit'] = 'required|unique:data_penyakit,kode_penyakit';
        }
       
        return $rules;
    }
}
