<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataJadwalPeriksaRequest;
use App\Models\DataJadwalPeriksa;
use Illuminate\Http\Request;

class DataJadwalPeriksaController extends Controller
{
    public function index()
    {
        $dataJadwalPeriksa = DataJadwalPeriksa::latest()->get();
        
        return view('admin.data-jadwal-periksa.index', compact('dataJadwalPeriksa'));
    }

    public function create()
    {
        return view('admin.data-jadwal-periksa.create');
    }

    public function store(DataJadwalPeriksaRequest $request)
    {
        DataJadwalPeriksa::create([
            'user_id' => auth()->user()->id,
            'tanggal_periksa' => $request->tanggal_periksa
        ]);

        return redirect()->route('data-jadwal-periksa.index')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function destroy(DataJadwalPeriksa $dataJadwalPeriksa)
    {
        $dataJadwalPeriksa->delete();

        return redirect()->route('data-jadwal-periksa.index')->with('destroy', 'Data Berhasil Dihapus!');
    }
}
