<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataPenyakitRequest;
use App\Models\DataPenyakit;
use Illuminate\Http\Request;

class DataPenyakitController extends Controller
{
    public function index()
    {
        $dataPenyakit = DataPenyakit::latest()->get();

        return view('admin.data-penyakit.index', compact('dataPenyakit'));
    }

    public function create()
    {
        return view('admin.data-penyakit.create');
    }

    public function store(DataPenyakitRequest $request)
    {
        DataPenyakit::create($request->all());

        return redirect()->route('data-penyakit.index')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function edit(DataPenyakit $dataPenyakit)
    {
        return view('admin.data-penyakit.edit', compact('dataPenyakit'));
    }

    public function update(DataPenyakitRequest $request, DataPenyakit $dataPenyakit)
    {
        $dataPenyakit->update($request->all());

        return redirect()->route('data-penyakit.index')->with('update', 'Data Berhasil Diupdate!');
    }

    public function destroy(DataPenyakit $dataPenyakit)
    {
        $dataPenyakit->delete();

        return redirect()->route('data-penyakit.index')->with('destroy', 'Data Berhasil Dihapus!');
    }
}
