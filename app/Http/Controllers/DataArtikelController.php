<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataArtikelRequest;
use App\Models\DataArtikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DataArtikelController extends Controller
{
    public function index()
    {
        $dataArtikel = DataArtikel::latest()->get();

        return view('admin.data-artikel.index', compact('dataArtikel'));
    }

    public function create()
    {
        return view('admin.data-artikel.create');
    }

    public function store(DataArtikelRequest $request)
    {
        $gambar = $request->file('gambar');
        $nama_gambar = time()."_".$gambar->getClientOriginalName();
        $gambar->storeAs('public/images', $nama_gambar);

        DataArtikel::create([
            'judul_artikel' => $request->judul_artikel,
            'deskripsi' => $request->deskripsi,
            'gambar' => $nama_gambar
        ]);

        return redirect()->route('data-artikel.index')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function edit(DataArtikel $dataArtikel)
    {
        return view('admin.data-artikel.edit', compact('dataArtikel'));
    }

    public function update(DataArtikelRequest $request, DataArtikel $dataArtikel)
    {   
        $dataArtikel->update($request->all());

        if($request->hasFile('gambar')){
            Storage::delete('public/images/'.$dataArtikel->gambar);
            $gambar = $request->file('gambar');
            $nama_gambar = time()."_".$gambar->getClientOriginalName();
            $gambar->storeAs('public/images', $nama_gambar);
            $dataArtikel->gambar = $nama_gambar;
            $dataArtikel->update();            
        }

        return redirect()->route('data-artikel.index')->with('update', 'Data Berhasil Diupdate!');
    }

    public function destroy(DataArtikel $dataArtikel)
    {
        $dataArtikel->delete();
        Storage::delete('public/images/'.$dataArtikel->gambar);

        return redirect()->route('data-artikel.index')->with('destroy', 'Data Berhasil Dihapus!');
    }
}
