<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataGejalaRequest;
use App\Models\DataGejala;
use Illuminate\Http\Request;

class DataGejalaController extends Controller
{
    public function index()
    {
        $dataGejala = DataGejala::latest()->get();

        return view('admin.data-gejala.index', compact('dataGejala'));
    }

    public function create()
    {
        return view('admin.data-gejala.create');
    }

    public function store(DataGejalaRequest $request)
    {
        DataGejala::create($request->all());

        return redirect()->route('data-gejala.index')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function edit(DataGejala $dataGejala)
    {
        return view('admin.data-gejala.edit', compact('dataGejala'));
    }

    public function update(DataGejalaRequest $request, DataGejala $dataGejala)
    {
        $dataGejala->update($request->all());

        return redirect()->route('data-gejala.index')->with('update', 'Data Berhasil Diupdate!');
    }

    public function destroy(DataGejala $dataGejala)
    {
        $dataGejala->delete();

        return redirect()->route('data-gejala.index')->with('destroy', 'Data Berhasil Dihapus!');
    }
}
