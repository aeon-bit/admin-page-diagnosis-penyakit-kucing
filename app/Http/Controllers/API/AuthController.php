<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    //
    public function login (Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
     
        $user = User::where('email', $request->email)->first();
     
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Email tidak terdaftar.'],
            ]);
        }
     
        $token = $user->createToken("token")->plainTextToken;
        return response() -> json([
            "response" => "ok",
            "token" => $token,

            "data" => $user
        ]);
    }

    public function register (Request $request) {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required',
            'nama_kucing' => 'required',
            'jenis_kucing' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);
     
        $user = new User;
        $user->nama = $request->nama;
        $user->alamat = $request->alamat;
        $user->no_hp = $request->no_hp;
        $user->username = $request->nama;
        $user->nama_kucing = $request->nama_kucing;
        $user->jenis_kucing = $request->jenis_kucing;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();
     
        return response() -> json([
            "response" => "ok",

            "data" => $user
        ]);
    }
}
