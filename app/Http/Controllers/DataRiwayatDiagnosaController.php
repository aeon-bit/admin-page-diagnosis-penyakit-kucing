<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataRiwayatDiagnosaRequest;
use App\Models\DataGejala;
use App\Models\DataJadwalPeriksa;
use App\Models\DataRiwayatDiagnosa;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class DataRiwayatDiagnosaController extends Controller
{
    public function index()
    {
        $dataRiwayatDiagnosa = DataRiwayatDiagnosa::latest()->get();

        return view('admin.data-riwayat-diagnosa.index', compact('dataRiwayatDiagnosa'));
    }

    public function create()
    {
        $dataGejala = DataGejala::all();
        $dataJadwalPeriksa = DataJadwalPeriksa::doesnthave('dataRiwayatDiagnosa')->get();

        return view('admin.data-riwayat-diagnosa.create', compact('dataGejala', 'dataJadwalPeriksa'));
    }

    public function store(DataRiwayatDiagnosaRequest $request)
    {
        $gejala = [];
        foreach ($request->gejala as $item) {
            $gejala[] = [
                'gejala' => $item
            ];
        }
        
        DataRiwayatDiagnosa::create([
            'data_jadwal_periksa_id' => $request->data_jadwal_periksa_id,
            'gejala' => $gejala,
            'hasil_diagnosa' => $request->hasil_diagnosa,
            'kesimpulan_diagnosa' => $request->kesimpulan_diagnosa,
            'saran_pengobatan' => $request->saran_pengobatan
        ]);

        return redirect()->route('data-riwayat-diagnosa.index')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function show(DataRiwayatDiagnosa $dataRiwayatDiagnosa)
    {
        return view('admin.data-riwayat-diagnosa.show', compact('dataRiwayatDiagnosa'));
    }

    public function pdf(DataRiwayatDiagnosa $dataRiwayatDiagnosa)
    {   
        // return view('admin.data-riwayat-diagnosa.cetak', compact('dataRiwayatDiagnosa'));
        $pdf = Pdf::loadView('admin.data-riwayat-diagnosa.cetak', compact('dataRiwayatDiagnosa'));
        return $pdf->download('data-riwayat-diagnosa.pdf');
    }
}
