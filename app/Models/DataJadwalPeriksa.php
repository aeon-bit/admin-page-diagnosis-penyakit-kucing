<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataJadwalPeriksa extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'data_jadwal_periksa';
    protected $with = 'dataRiwayatDiagnosa';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function dataRiwayatDiagnosa()
    {
        return $this->hasOne(DataRiwayatDiagnosa::class, 'data_jadwal_periksa_id');
    }
}
