<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataRiwayatDiagnosa extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'data_riwayat_diagnosa';
    protected $casts = [
        'gejala' => 'array',
        ];

    public function dataJadwalPeriksa()
    {
        return $this->belongsTo(DataJadwalPeriksa::class, 'data_jadwal_periksa_id');
    }
}
