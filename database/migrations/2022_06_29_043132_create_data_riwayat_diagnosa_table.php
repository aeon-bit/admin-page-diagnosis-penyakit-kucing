<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataRiwayatDiagnosaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_riwayat_diagnosa', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('data_jadwal_periksa_id');
            $table->json('gejala');
            $table->string('hasil_diagnosa');
            $table->text('kesimpulan_diagnosa');
            $table->text('saran_pengobatan');
            $table->timestamps();
            
            $table->foreign('data_jadwal_periksa_id')->references('id')->on('data_jadwal_periksa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_riwayat_diagnosa');
    }
}
