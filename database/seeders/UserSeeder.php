<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama' => 'admin',
            'email' => 'admin@admin.com',
            'alamat' => 'alamat admin',
            'no_hp' => 999999999999,
            'username' => 'admin',
            'nama_kucing' => 'admin',
            'jenis_kucing' => 'admin',
            'password' => bcrypt('password'),
            'isAdmin' => true
        ]);
    }
}
